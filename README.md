This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to Run

```
    git clone https://sagsag@bitbucket.org/sagsag/candidate-evaluation.git
    npm install
    npm start
```