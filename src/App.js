import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import { withStyles } from '@material-ui/core/styles';
import ReactPlayer from 'react-player'

import addSubtitlesToVideo from "add-subtitles-to-video";

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  icon: {
    marginRight: theme.spacing.unit * 2,
  },
  heroUnit: {
    backgroundColor: theme.palette.background.paper,
  },
  heroContent: {
    maxWidth: 600,
    margin: '0 auto',
    padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
  },
  heroButtons: {
    marginTop: theme.spacing.unit * 4,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit * 6,
  },
});

const Video = (props) => {
  if (typeof props.src !== "undefined" && props.src !== "") {
    return <ReactPlayer url={props.src} controls={true} width="100%" playing />
  } 
  
  return null
}

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      cloudName: "",
      videoID: "",
      subtitles: "",
      output: "",
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    console.log(event.target.id)
    this.setState({[event.target.id]: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    
    const ob = JSON.parse(this.state.subtitles)
    this.setState({
      output: addSubtitlesToVideo(this.state.videoID, ob).replace("candidate-evaluation", this.state.cloudName)
    })
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" color="inherit" noWrap>
              Add Subtitles To Video
            </Typography>
          </Toolbar>
        </AppBar>
        <main>
          {/* Hero unit */}
          <div className={classes.heroUnit}>
            <div className={classes.heroContent}>
              <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                Welcome to Adding Subtitles to Video
              </Typography>
              <Typography variant="h6" align="center" color="textSecondary" paragraph>
                Make sure you have your Cloudinary cloud name ready, your video's Public ID, and finally a JSON containing the subtitles you want to add.
              </Typography>
            </div>
          </div>
          <div className={classNames(classes.layout, classes.cardGrid)}>
            {/* End hero unit */}
            <Grid container>
            <Grid item xs={2}></Grid>
              <Grid item xs={8}>
                <Video src={this.state.output} />
              </Grid>
              <Grid item xs={2}></Grid>

              <Grid item xs={2}></Grid>
              <Grid item xs={8}>
                <TextField fullWidth id="cloudName" type="input" label="Cloud Name" margin="normal" value={this.state.cloudName} onChange={this.handleChange} />
              </Grid>
              <Grid item xs={2}></Grid>
  
              <Grid item xs={2}></Grid>
              <Grid item xs={8}>
                <TextField fullWidth id="videoID" type="input" label="Video ID" margin="normal" value={this.state.videoID} onChange={this.handleChange} />
              </Grid>
              <Grid item xs={2}></Grid>
  
              <Grid item xs={2}></Grid>
              <Grid item xs={8}>
                <TextField  id="subtitles" label="Subtitles JSON" multiline margin="normal" rows="12" className={classes.textField} fullWidth value={this.state.subtitles} onChange={this.handleChange} />
              </Grid>
              <Grid item xs={2}></Grid>
              
              <Grid item xs={2}></Grid>
              <Grid item xs={8}>
              <Button variant="contained" color="primary" className={classes.button} fullWidth onClick={this.handleSubmit}>
                Submit
              </Button>
              </Grid>
              <Grid item xs={2}></Grid>
            </Grid>
          </div>
        </main>
      </React.Fragment>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App)